﻿using System.Reflection;
using Autofac;
using Autofac.Integration.Mvc;
using RestaurantHelper.Dao.DbConnection;
using RestaurantHelper.Dao.DbModels;
using RestaurantHelper.Dao.Implementation;
using RestaurantHelper.Dao.Interfaces;
using RestaurantHelper.Services.Implementation;
using RestaurantHelper.Services.Interfaces;

namespace RestaurantHelper.Web.Infrastructure
{
    public static class Binder
    {
        public static IContainer GetContainer()
        {
            ContainerBuilder builder = new ContainerBuilder();

            builder.RegisterType<MyDbContext>().AsSelf().SingleInstance();

            builder.RegisterControllers(Assembly.GetExecutingAssembly()).InstancePerLifetimeScope();

            builder.RegisterType<FoodRepository>().OnActivating(n =>
            {
                n.Instance.LoadDbContext(new MyDbContext());
            }).As<IRepository<Food>>();

            builder.Register(n =>
            {
                FoodRepository repository = new FoodRepository();
                MyDbContext dbContext = n.Resolve<MyDbContext>();
                repository.LoadDbContext(dbContext);

                return repository;
            }).As<IRepository<Food>>();

            builder.Register(n =>
            {
                LocationRepository repository = new LocationRepository();
                MyDbContext dbContext = n.Resolve<MyDbContext>();
                repository.LoadDbContext(dbContext);

                return repository;
            }).As<IRepository<Location>>();

            builder.Register(n =>
            {
                RestaurantRepository repository = new RestaurantRepository();
                MyDbContext dbContext = n.Resolve<MyDbContext>();
                repository.LoadDbContext(dbContext);

                return repository;
            }).As<IRepository<Restaurant>>();

            builder.Register(n =>
            {
                RestaurantFoodRepository repository = new RestaurantFoodRepository();
                MyDbContext dbContext = n.Resolve<MyDbContext>();
                repository.LoadDbContext(dbContext);

                return repository;
            }).As<IRepository<RestaurantFood>>();

            builder.RegisterType<FoodService>().PropertiesAutowired().As<IFoodService>();
            builder.RegisterType<RestaurantService>().PropertiesAutowired().AutoActivate().As<IRestaurantService>();
            builder.RegisterType<RestaurantFoodService>().PropertiesAutowired().AutoActivate().As<IRestaurantFoodService>();
            builder.RegisterType<LocationService>().PropertiesAutowired().AutoActivate().As<ILocationService>();

            return builder.Build();
        }
    }
}