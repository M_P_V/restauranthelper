﻿using System.Web.Mvc;
using RestaurantHelper.Services.Interfaces;

namespace RestaurantHelper.Web
{
    public class LocationsController : Controller
    {
        private ILocationService service;

        public LocationsController(ILocationService service)
        {
            this.service = service;
        }

        public ActionResult Index()
        {
            return View(service.GetEntities());
        }
    }
}
