﻿using System.Web.Mvc;
using RestaurantHelper.Services.Interfaces;

namespace RestaurantHelper.Web
{
    public class RestaurantsController : Controller
    {
        private IRestaurantService service;

        public RestaurantsController(IRestaurantService service)
        {
            this.service = service;
        }

        public ActionResult Index()
        {
            return View(service.GetEntities());
        }
    }
}
