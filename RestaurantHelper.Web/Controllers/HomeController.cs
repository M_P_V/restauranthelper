﻿using System.Web.Mvc;

namespace RestaurantHelper.Web.Controllers
{
    public class HomeController:Controller
    {
        public ActionResult Index()
        {
            return View();
        }
    }
}