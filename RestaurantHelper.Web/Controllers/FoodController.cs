﻿using System.Web.Mvc;
using RestaurantHelper.Services.Interfaces;

namespace RestaurantHelper.Web.Controllers
{
    public class FoodController:Controller
    {
        private IFoodService service;

        public FoodController(IFoodService service)
        {
            this.service = service;
        }

        public ActionResult Index()
        {
            return View(service.GetEntities());
        }
    }
}
