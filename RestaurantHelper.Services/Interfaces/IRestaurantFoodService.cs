﻿using RestaurantHelper.Dao.DbModels;

namespace RestaurantHelper.Services.Interfaces
{
    public interface IRestaurantFoodService:IService<RestaurantFood>
    {
    }
}
