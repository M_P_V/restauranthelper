﻿using RestaurantHelper.Dao.DbModels;

namespace RestaurantHelper.Services.Interfaces
{
    public interface ILocationService:IService<Location>
    {
    }
}
