﻿using RestaurantHelper.Dao.DbModels;
using RestaurantHelper.Dao.Interfaces;

namespace RestaurantHelper.Services.Interfaces
{
    public interface IFoodService: IService<Food>
    {
    }
}
