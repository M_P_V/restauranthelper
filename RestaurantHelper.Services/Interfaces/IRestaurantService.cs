﻿using RestaurantHelper.Dao.DbModels;
using System.Collections.Generic;

namespace RestaurantHelper.Services.Interfaces
{
    public interface IRestaurantService:IService<Restaurant>
    {
    }
}
