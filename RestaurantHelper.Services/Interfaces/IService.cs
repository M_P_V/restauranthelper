﻿using System.Collections.Generic;

namespace RestaurantHelper.Services.Interfaces
{
    public interface IService<T>
    {
        IEnumerable<T> GetEntities();
        T GetEntityById(int id);
    }
}
