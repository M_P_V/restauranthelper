﻿using RestaurantHelper.Dao.DbModels;
using RestaurantHelper.Dao.Interfaces;
using RestaurantHelper.Services.Interfaces;
using System.Collections.Generic;

namespace RestaurantHelper.Services.Implementation
{
    public class LocationService : ILocationService
    {
        private IRepository<Location> locationRepository;

        public IRepository<Location> LocationRepository
        {
            set
            {
                this.locationRepository = value;
            }
        }

        public IEnumerable<Location> GetEntities()
        {
            return locationRepository.GetAllEntities();
        }

        public Location GetEntityById(int id)
        {
            return locationRepository.GetEntityById(id);
        }
    }
}
