﻿using RestaurantHelper.Dao.DbModels;
using RestaurantHelper.Dao.Interfaces;
using RestaurantHelper.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantHelper.Services.Implementation
{
    public class RestaurantFoodService : IRestaurantFoodService
    {
        private IRepository<RestaurantFood> restaurantFoodRepository;
        public IRepository<RestaurantFood> RestaurantFoodRepository
        {
            set
            {
                this.restaurantFoodRepository = value; 
            }
        }

        public IEnumerable<RestaurantFood> GetEntities()
        {
            return restaurantFoodRepository.GetAllEntities();
        }

        public RestaurantFood GetEntityById(int id)
        {
            return restaurantFoodRepository.GetEntityById(id);
        }
    }
}
