﻿using System.Collections.Generic;
using RestaurantHelper.Dao.DbModels;
using RestaurantHelper.Dao.Interfaces;
using RestaurantHelper.Services.Interfaces;

namespace RestaurantHelper.Services.Implementation
{
    public class FoodService : IFoodService
    {
        private IRepository<Food> foodRepository { get; set; }
        public IRepository<Food> FoodRepository { set { this.foodRepository = value; } }

        public IEnumerable<Food> GetEntities()
        {
            return foodRepository.GetAllEntities();
        }

        public Food GetEntityById(int id)
        {
            return foodRepository.GetEntityById(id);
        }
    }
}
