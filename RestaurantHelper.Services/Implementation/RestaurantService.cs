﻿using RestaurantHelper.Dao.DbModels;
using RestaurantHelper.Dao.Interfaces;
using RestaurantHelper.Services.Interfaces;
using System.Collections.Generic;

namespace RestaurantHelper.Services.Implementation
{
    public class RestaurantService : IRestaurantService
    {
        private IRepository<Restaurant> restaurantRepository;

        public IRepository<Restaurant> RestaurantRepository
        {
            set
            {
                this.restaurantRepository = value;
            }
        }

        public IEnumerable<Restaurant> GetEntities()
        {
            return restaurantRepository.GetAllEntities();
        }

        public Restaurant GetEntityById(int id)
        {
            return restaurantRepository.GetEntityById(id);
        }
    }
}
