﻿using System.Collections.Generic;

namespace RestaurantHelper.Dao.Interfaces
{
    public interface IRepository<T>
    {
        IEnumerable<T> GetAllEntities();

        T GetEntityById(int id);
    }
}
