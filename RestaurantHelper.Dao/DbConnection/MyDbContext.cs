﻿using RestaurantHelper.Dao.DbModels;
using System.Data.Entity;

namespace RestaurantHelper.Dao.DbConnection
{
   public  class MyDbContext : DbContext
    {
        public DbSet<Restaurant> Restaurants { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<RestaurantFood> RestaurantsFood { get; set; }
        public DbSet<Food> Food { get; set; }

        public MyDbContext(string connectionString) : base(connectionString) { }

        public MyDbContext() : base("SqlServer") { }
    }
}
