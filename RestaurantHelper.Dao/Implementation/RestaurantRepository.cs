﻿using System.Collections.Generic;
using System.Linq;
using RestaurantHelper.Dao.DbModels;
using RestaurantHelper.Dao.Interfaces;

namespace RestaurantHelper.Dao.Implementation
{
    public class RestaurantRepository : RepositoryBase, IRepository<Restaurant>
    {
        public IEnumerable<Restaurant> GetAllEntities()
        {
            return dbContext.Restaurants;
        }

        public Restaurant GetEntityById(int id)
        {
            return dbContext.Restaurants.FirstOrDefault(n => n.Id == id);
        }
    }
}
