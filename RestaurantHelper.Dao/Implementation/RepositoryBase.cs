﻿using RestaurantHelper.Dao.DbConnection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantHelper.Dao.Implementation
{
    public class RepositoryBase
    {
        protected MyDbContext dbContext { get; set; }

        public void LoadDbContext(MyDbContext dbContext)
        {
            this.dbContext = dbContext;
        }
    }
}
