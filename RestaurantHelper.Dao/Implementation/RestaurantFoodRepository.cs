﻿using RestaurantHelper.Dao.DbModels;
using RestaurantHelper.Dao.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RestaurantHelper.Dao.Implementation
{
    public class RestaurantFoodRepository : RepositoryBase, IRepository<RestaurantFood>
    {
        public IEnumerable<RestaurantFood> GetAllEntities()
        {
            return dbContext.RestaurantsFood;
        }

        public RestaurantFood GetEntityById(int id)
        {
            return dbContext.RestaurantsFood.FirstOrDefault(n => n.Id == id);
        }
    }
}
