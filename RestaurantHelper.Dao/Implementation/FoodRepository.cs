﻿using RestaurantHelper.Dao.DbConnection;
using RestaurantHelper.Dao.DbModels;
using RestaurantHelper.Dao.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantHelper.Dao.Implementation
{
    public class FoodRepository : RepositoryBase, IRepository<Food>
    {
        public IEnumerable<Food> GetAllEntities()
        {
            return dbContext.Food;
        }

        public Food GetEntityById(int id)
        {
            return dbContext.Food.FirstOrDefault(n => n.Id == id);
        }
    }
}
