﻿using RestaurantHelper.Dao.DbModels;
using RestaurantHelper.Dao.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace RestaurantHelper.Dao.Implementation
{
    public class LocationRepository : RepositoryBase, IRepository<Location>
    {
        public IEnumerable<Location> GetAllEntities()
        {
            return dbContext.Locations;
        }

        public Location GetEntityById(int id)
        {
            return dbContext.Locations.FirstOrDefault(n => n.Id == id);
        }
    }
}
