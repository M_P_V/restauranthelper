namespace RestaurantHelper.Dao.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CreateSeed : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Foods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Locations",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Country = c.String(),
                        City = c.String(),
                        Street = c.String(),
                        HouseNumber = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Restaurants",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Location_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Locations", t => t.Location_Id)
                .Index(t => t.Location_Id);
            
            CreateTable(
                "dbo.RestaurantFoods",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Food_Id = c.Int(),
                        Restaurant_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Foods", t => t.Food_Id)
                .ForeignKey("dbo.Restaurants", t => t.Restaurant_Id)
                .Index(t => t.Food_Id)
                .Index(t => t.Restaurant_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.RestaurantFoods", "Restaurant_Id", "dbo.Restaurants");
            DropForeignKey("dbo.RestaurantFoods", "Food_Id", "dbo.Foods");
            DropForeignKey("dbo.Restaurants", "Location_Id", "dbo.Locations");
            DropIndex("dbo.RestaurantFoods", new[] { "Restaurant_Id" });
            DropIndex("dbo.RestaurantFoods", new[] { "Food_Id" });
            DropIndex("dbo.Restaurants", new[] { "Location_Id" });
            DropTable("dbo.RestaurantFoods");
            DropTable("dbo.Restaurants");
            DropTable("dbo.Locations");
            DropTable("dbo.Foods");
        }
    }
}
