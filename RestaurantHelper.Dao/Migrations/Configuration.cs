using RestaurantHelper.Dao.DbModels;
using System.Data.Entity.Migrations;
using AutoFixture;

namespace RestaurantHelper.Dao.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DbConnection.MyDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(DbConnection.MyDbContext context)
        {
            int entitiesCount = 20;

            Fixture entityProvider = new Fixture();
            for (int i = 0; i < entitiesCount; i++)
            {
                Food food = entityProvider.Create<Food>();
                Location location = entityProvider.Create<Location>();
                Restaurant restaurant = entityProvider.Create<Restaurant>();
                RestaurantFood restaurantFood = entityProvider.Create<RestaurantFood>();

                food.Id = i;
                location.Id = i;
                restaurant.Id = i;
                restaurantFood.Food = food;
                restaurantFood.Restaurant= restaurant;
                restaurant.Location = location;

                context.Food.Add(food);
                context.Locations.Add(location);
                context.Restaurants.Add(restaurant);
                context.RestaurantsFood.Add(restaurantFood);

                context.SaveChanges();
            }
        }
    }
}
