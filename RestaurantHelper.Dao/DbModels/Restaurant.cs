﻿using System.Collections.Generic;

namespace RestaurantHelper.Dao.DbModels
{
    public class Restaurant
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public Location Location { get; set; }
    }
}
