﻿namespace RestaurantHelper.Dao.DbModels
{
    public class RestaurantFood
    {
        public int Id { get; set; }
        public Restaurant Restaurant { get; set; }
        public Food Food { get; set; }
    }
}
