﻿namespace RestaurantHelper.Dao.DbModels
{
    public class Food
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
    }
}
